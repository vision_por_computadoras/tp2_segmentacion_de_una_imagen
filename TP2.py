import cv2
import numpy

#Leo la imagen
img = cv2.imread('hoja.png', 0)

img2=[]
temp=[]
limite=250

for row  in img: 
    for cols in row:
        if cols < limite:
            temp.append(0)      #Si el color es mas oscuro que el limite coloco un 0, o un pixel negro
        else:
            temp.append(255)    #Si el color es mas claro que el limite coloco un 255, o un pixel blanco
    img2.append(temp)
    temp=[]
    

cv2.imwrite('imgtrafo.png', numpy.array(img2))
